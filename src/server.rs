use actix_web::{web, App, HttpServer};
use actix_web_httpauth::extractors::basic::{Config};
use log::{info, error};
use crate::entities::{Client, CmkClientServerConfig};
use crate::handler::index;

#[derive(Debug)]
pub struct AppState {
    pub clients: Vec<Client>,
    pub client_output_folder: String,
}


pub async fn run_server(app_config: CmkClientServerConfig) {
    info!("Starting server");
    match launch_server(app_config).await {
        Ok(_) => {
            info!("Server terminated");
        }
        Err(e) => {
            error!("Failed to run server {}", e);
        }
    }
}

async fn launch_server(app_config: CmkClientServerConfig) -> std::io::Result<()> {
    let c = app_config.clients;
    let output_folder = app_config.client_output_folder;
    HttpServer::new(move || App::new()
        .data(Config::default().realm("Restricted area"))
        .data(AppState {
            clients: c.clone(),
            client_output_folder: output_folder.clone(),
        })
        .app_data(web::PayloadConfig::new(10000000))
        .service(web::resource("/").route(web::post().to(index))))
        .bind(app_config.bind_address)?
        .workers(1)
        .run()
        .await
}
