use serde::{Serialize, Deserialize};

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Client {
    pub id: String,
    pub password: String,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct CmkClientServerConfig {
    pub clients: Vec<Client>,
    pub bind_address: String,
    pub client_output_folder: String,
}

impl ::std::default::Default for CmkClientServerConfig {
    fn default() -> Self {
        Self {
            clients: Vec::new(),
            bind_address: String::from("127.0.0.1:8080"),
            client_output_folder: String::from("/var/cmk-mk-client-server/output"),
        }
    }
}