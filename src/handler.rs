use actix_web::{web, HttpResponse, Responder, HttpRequest};
use actix_web_httpauth::extractors::basic::{BasicAuth};
use crate::server::AppState;
use std::io::Write;
use chrono;
use std::fs::File;
use chrono::{DateTime, Local};
use log::{info, warn, error, debug};

const LAST_CLIENT_UPDATE: &str = "cmk_client_last_update";
const LAST_CLIENT_REMOTE_ADDRESS: &str = "cmk_client_last_remote_address";

pub async fn index(auth: BasicAuth, req_body: String, req: HttpRequest, data: web::Data<AppState>) -> impl Responder {
    if !is_client_authenticated(&auth, &data) {
        warn!("Bad authentication with {} {:?}", auth.user_id(), auth.password());
        return HttpResponse::Forbidden().finish();
    }

    let lines = req_body.lines();

    for line in lines {
        if line.starts_with("<<<<") && line.ends_with(">>>>") {
            warn!("client tries to send piggyback data");
            return HttpResponse::BadRequest().body("piggyback data not allowed");
        }
    }

    let mut path  = data.client_output_folder.clone();
    path.push_str("/");
    path.push_str(auth.user_id());
    match std::fs::File::create(&path) {

        Ok(mut output_file) => {
            match write_file(req_body, &req.connection_info().remote_addr(),&mut output_file) {
                Ok(_) => {
                    info!("received data from {}", auth.user_id());
                    return HttpResponse::Ok().finish();
                }
                Err(e) => {
                    debug!("Failed to write in output file, {:?}", e);
                    return HttpResponse::InternalServerError().finish();
                }
            }
        }

        Err(e) => {
            error!("Failed to open file {}, error {:?}", &path, e);
            return HttpResponse::InternalServerError().finish();
        }
    }
}

fn write_file(req_body: String, remote_address: &Option<&str>, output_file: &mut File) -> std::io::Result<()> {
    let date_time = chrono::offset::Local::now();
    let mut found_local_section = false;
    for line in req_body.lines() {
        output_file.write_all(line.as_bytes())?;
        output_file.write_all("\n".as_bytes())?;
        if line.eq("<<<local>>>") {
            write_local_checks(remote_address, output_file, date_time)?;
            found_local_section = true;
        }
    }
    if !found_local_section {
        output_file.write("<<<local>>>\n".as_bytes())?;
        write_local_checks(remote_address, output_file, date_time)?;
    }
    output_file.sync_all()
}

fn write_local_checks(remote_address: &Option<&str>, output_file: &mut File, date_time: DateTime<Local>) -> std::io::Result<()> {
    output_file.write_all(format!("0 {} - OK: Last seen {}\n", &LAST_CLIENT_UPDATE, date_time).as_bytes())?;
    output_file.write_all(format!("0 {} - OK: Client address {}\n", &LAST_CLIENT_REMOTE_ADDRESS, remote_address.unwrap_or(&"unknown")).as_bytes())?;
    Ok(())
}

fn is_client_authenticated(auth: &BasicAuth, data: &web::Data<AppState>) -> bool {
    let mut client_authenticated = false;

    // check credentials
    for client in &data.clients {
        if auth.user_id().eq(&client.id) {
            match auth.password() {
                Some(password) => {
                    if password.eq(&client.password) {
                        client_authenticated = true;
                        break;
                    }
                }
                None => {
                    // go on
                }
            }
        }
    }

    return client_authenticated;
}
