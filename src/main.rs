mod server;
mod entities;
mod handler;

use crate::server::run_server;
use crate::entities::{CmkClientServerConfig};

fn load_config() -> Result<CmkClientServerConfig, confy::ConfyError> {
    let cfg = confy::load_path("/etc/cmk-mk-client-server/config");
    return cfg;
}

#[actix_web::main]
async fn main() {

    env_logger::builder().target(env_logger::Target::Stdout).init();

    let result = load_config();

    match result {
        Ok(cfg) => {
            run_server(cfg).await;
        }
        Err(e) => {
            eprintln!("Failed to load config {}", e);
        }
    }
}

